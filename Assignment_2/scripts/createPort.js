var apikey = '01995462fad84940a8ea88df2332d572';
var api_url = 'https://api.opencagedata.com/geocode/v1/json'
var addressName, lat, lng;
mapboxgl.accessToken = 'pk.eyJ1IjoiamlhYmluIiwiYSI6ImNrOWRwOGpiZjA0YWwzZWxsM3J5Z280aXoifQ.t_1iM47Zq06wEArTyO2rJQ';

let PORT_STORAGE_KEY = "PORT_STORAGE_BY_USER";


let openCageData = {
    q:null,
    key:apikey,
    language:"en",
    pretty:"1",
    no_annotations:"1",
    callback:"coordsResponse"   
}


function coordsResponse(data){
     
     lat = data.results[0].geometry.lat;
       
     lng = data.results[0].geometry.lng;
       
    addressName = data.results[0].formatted;
    
    //show the map when save
    
    
    var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
    center: [-74.5, 40], // starting position [lng, lat]
    zoom: 9 // starting zoom
    });
    
    //mark the location of port
    marker = new mapboxgl.Marker()
      .setLngLat([lng, lat])
      .addTo(map);
    //panto the marker
    map.panTo([lng, lat])
        
    //popup the marker
    var popup = new mapboxgl.Popup({ closeOnClick: false })

    .setLngLat([lng, lat])

    .setHTML(addressName)

    .addTo(map);
}


    
function createPort (){
    //This function will store the user input then creat a new port object then it would be store in local storage finally.
    let portName = document.getElementById("portName").value;  
    let portCountry = document.getElementById("portCountry").value;
    let portType = document.getElementById("portType").value;
    let size = document.getElementById("size").value;
    let locationRef = document.getElementById("location").value;
    let loc = locationRef.split(',');
    let latitude = loc[0];
    let longitude = loc[1];
    openCageData.q = portName;
    jsonpRequest(api_url, openCageData)
    //store the information of the port
    let newPort = new Port(portName, portCountry, portType, size, latitude, longitude)
    
    portList.addPortFromBeginning(newPort);
    
    localStorage.setItem(PORT_STORAGE_KEY, JSON.stringify(portList._portList));

    
    
}

