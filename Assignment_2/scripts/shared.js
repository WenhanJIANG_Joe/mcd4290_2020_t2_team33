//A ship should have a name, maximum speed (in knots), range (in km), description, cost (per km) and a status.

//A port should have a name, country, type, size, latitude and longitude.

//A route should have a name, ship, sourcePort, destinationPort, distance, time, cost, startDate and wayPointList. The wayPointList will be an array that holds location objects.
 
class Ship{
    
    constructor(name, maxSpeed, range, description, cost){
        this._name = name;
        this._maxSpeed = maxSpeed;
        this._range = range;
        this._description = description;
        this._cost = cost;
        this._status = "available";
        this._comments = "";
    }
    
    //Getter
    get name(){
        return this._name;
    }
    
    get maxSpeed(){
        return this._maxSpeed;
    }
    
    get range(){
        return this._range;
    }
    
    get description(){
        return this._description;
    }
    
    get cost(){
        return this._cost;
    }
    
    get status(){
        return this._status;
    }
    
     _shipInitalize(shipObject){
         this._name = shipObject._name;
         this._maxSpeed = shipObject._maxSpeed;
         this._range = shipObject._range;
         this._description = shipObject._description;
         this._cost = shipObject._cost;
         this._status = shipObject._status;
         this._comments = shipObject._comments;
     }
    
    
}


class Port{
    
    constructor(name, country, type, size, latitude, longitude){
        this._name = name;
        this._country = country;
        this._type = type;
        this._size = size;
        this._locprecision = "Unknown";
        this._latitude = latitude;
        this._longitude = longitude;
    }
    
    //Getter
    get name(){
        return this._name;
    }
    
    get country(){
        return this._country;
    }
    
    get type(){
        return this._type;
    }
    
    get size(){
        return this._size;
    }
    
    get latitude(){
        return this._latitude;
    }
    
    get longitude(){
        return this._longitude;
    }
    
    _portInitalize(portObject){
         this._name = portObject._name;
         this._country = portObject._country;
         this._type = portObject._type;
         this._size = portObject._size;
         this._locprecision = portObject._locprecision;
         this._latitude = portObject._latitude;
         this._longitude = portObject._longitude;
     }
    
}



class Route{
    
    constructor(name, ship, sourcePort, destinationPort, distance, time, cost, startDate, wayPointList, startPortName, destinationPortName){
        this._name = name;
        this._ship = ship;
        this._sourcePort = sourcePort;
        this._destinationPort = destinationPort;
        this._distance = distance;
        this._time = time;
        this._cost = cost;
        this._startDate = startDate;
        this._wayPointList = wayPointList;
        this._startPortName = startPortName;
        this._destinationPortName = destinationPortName;
    }
    
    get name(){
        return this._name;
    }
    
    get ship(){
        return this._ship;
    }
    
    get sourcePort(){
        return this._sourcePort;
    }
    
    get destinationPort(){
        return this._destinationPort;
    }
    
    get distance(){
        return this._distance;
    }
    
    get time(){
        return this._time;
    }
    
    get cost(){
        return this._cost;
    }
    
    get startDate(){
        return this._startDate;
    }
    
    get wayPointList(){
        return this._wayPointList;
    }
    
    get startPortName(){
        return this._startPortName;
    }
    
    get destinationPortName(){
        return this._destinationPortName;
    }
}


class ShipList{
    
    constructor(){
        this._shipList = [];
    }
    
    addShip(ship){
        this._shipList.push(ship);
    }
    
    returnLength(){
        return this._shipList.length;
    }
    
    getList(){
        return this._shipList;
    }
}


class PortList{
    
    constructor(){
        this._portList = [];
    }
    
    addPort(port){
        this._portList.push(port);
    }
    
    addPortFromBeginning(port){
        this._portList.unshift(port);
    }
    
}


class RouteList{
    
    constructor(){
        this._routeList = [];
    }
    
    addRoute(route){
        this._routeList.push(route);
    }
}

let shipList = new ShipList();  
let portList = new PortList();
let routeList = new RouteList();

if(localStorage.getItem("SHIP_STORAGE") !== null){
    shipList._shipList = JSON.parse(localStorage.getItem("SHIP_STORAGE"));
}

if(localStorage.getItem("PORT_STORAGE_BY_USER") !== null){
    portList._portList = JSON.parse(localStorage.getItem("PORT_STORAGE_BY_USER"));
}

if(localStorage.getItem("ROUTE_STORAGE_BY_USER") !== null){
    routeList._routeList = JSON.parse(localStorage.getItem("ROUTE_STORAGE_BY_USER"));
}


//function request API
function jsonpRequest(url, data)
{
    // Build URL parameters from data object.
    let params = "";
    // For each key in data object...
    for (let key in data)
    {
        if (data.hasOwnProperty(key))
        {
            if (params.length == 0)
            {
                // First parameter starts with '?'
                params += "?";
            }
            else
            {
                // Subsequent parameter separated by '&'
                params += "&";
            }

            let encodedKey = encodeURIComponent(key);
            let encodedValue = encodeURIComponent(data[key]);

            params += encodedKey + "=" + encodedValue;
         }
    }
    let script = document.createElement('script');
    script.src = url + params;
    document.body.appendChild(script);
}

let shipAPIData = {
    callback: "callBackFunc"
}
















