let fromPortLocation = [];
let toPortLocation = [];
let totalDis = 0;
let time = 0;
let cost = 0;
let startPort = document.getElementById("startP");
let endPort = document.getElementById("endP");
let sourcePort = document.getElementById("sourcePort");
let destinationPort = document.getElementById("destinationPort");
let ports = JSON.parse(localStorage.getItem("allPort"));
let allOb = JSON.parse(localStorage.getItem('PORT_STORAGE'));
//console.log(allOb[1]);
let s = localStorage.getItem("startP");
let e = localStorage.getItem("endP");
let pointList = [null, null];
let marker1, marker2;
startPort.innerHTML += s;
endPort.innerHTML += e;


function fillSourcePort(){
    //this function will fill the drop down list of source port from local storage.
    let sP = document.getElementById("startP");
    let display = '<option></option>';
    let startPortRef = document.getElementById("sourcePort");
    for(let i = 0; i < allOb.length; i++){
        if(allOb[i]._country === sP.value){
            display += "<option>" + allOb[i]._name + "</option>";
        } 
    }
    startPortRef.innerHTML = display;
}

function fillDestinationPort(){
    //this function will fill the drop down list of destination port from local storage.
    let eP = document.getElementById("endP");
    let display = '<option></option>';
    let endPortRef = document.getElementById("destinationPort");
    for(let i = 0; i < allOb.length; i++){
        if(allOb[i]._country === eP.value){
            display += "<option>" + allOb[i]._name + "</option>";
        } 
    }
    endPortRef.innerHTML = display;
}

function fromLatAndLonRespons(){
    //this function will store the position of the starting port.
    fromPortLocation = []
    for(let i = 0; i < allOb.length; i++){
    
        if (sourcePort.value === allOb[i]._name){
        
            fromPortLocation.push(allOb[i]._longitude, allOb[i]._latitude)
            
    
        }
    }
    
    pointList[0] = fromPortLocation;
    console.log(fromPortLocation)
    console.log(pointList)
    console.log(sourcePort.value)
}

function toLatAndLonRespons(){
    //this function will sotre the position of the ending point.
    toPortLocation = []
    for(let i = 0; i < allOb.length; i++){
    
        if (destinationPort.value === allOb[i]._name){
            toPortLocation.push(allOb[i]._longitude, allOb[i]._latitude)
            marker1 = allOb[i]._longitude;
            marker2 = allOb[i]._latitude;
        }
    }
    pointList[1] = toPortLocation;
    console.log(toPortLocation)
    console.log(pointList)
}

function createRoute(){
    //let test = [];
    //this function will generate a map for displaying the route and user could click to add points then the function will call itself recursively.
    mapboxgl.accessToken = 'pk.eyJ1IjoiamlhYmluIiwiYSI6ImNrOWRwOGpiZjA0YWwzZWxsM3J5Z280aXoifQ.t_1iM47Zq06wEArTyO2rJQ';
    
    var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
   center: [0, 0], // starting position [lng, lat]
    zoom: 1 // starting zoom
    });
    
    map.on('load', function() {

        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': pointList
                }
            }
        });

        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#666',
                'line-width': 4
            }
        });
    
    //make a marker when click map; 
    map.on('click', addMarker);
    function addMarker(e){
            pointList.pop();
            pointList.push([e.lngLat.lng, e.lngLat.lat]);
            pointList.push([marker1, marker2]);
            console.log(pointList);
            if(checkPoint(pointList)===false){
                pointList.pop();
                pointList.pop();
                pointList.push([marker1, marker2]);
                alert("Each point need to further than 100Km");
            }
}
    map.on('click', createRoute);
    

});

    
    for(let i = 0; i < pointList.length; i++){
        if(i === 0){
            marker = new mapboxgl.Marker({color: 'green'})
            .setLngLat(pointList[i])
            .addTo(map)
        }
        else if(i === pointList.length-1){
            marker = new mapboxgl.Marker({color: 'red'})//should be red
              .setLngLat(toPortLocation)
              .addTo(map);
        }
        else{
            marker = new mapboxgl.Marker({ color: 'orange'})
            .setLngLat(pointList[i])
            .addTo(map)
        }
    }
    
    
    //calculate distance
    totalDis = 0;
    for(let i = 1; i < pointList.length; i++){
        totalDis += distanceCal(pointList[i-1][0], pointList[i-1][1], pointList[i][0], pointList[i][1])
    }
    console.log(totalDis);
    showAvailableShip(totalDis);
}

let shipArr = new Array;
let theShip = JSON.parse(localStorage.getItem("SHIP_STORAGE"));
if (theShip !== null){
    for(let i = 0; i < theShip.length; i++){
    let newShip = new Ship();
    newShip._shipInitalize(theShip[i]);
    shipArr.push(newShip);
    }
}

function showShip(ele){
    //this function will put available ship in a array in order to display them all later.
    for(let i = 0; i < ele['ships'].length; i++){
        if(ele['ships'][i].status==='available'){
            let aShip = new Ship(ele['ships'][i].name, ele['ships'][i].maxSpeed, ele['ships'][i].range, ele['ships'][i].desc, ele['ships'][i].cost);
            shipArr.push(aShip);
        }
    }
    //console.log(shipArr);
}

var url = "https://eng1003.monash/api/v1/ships/?callback=showShip";
var script = document.createElement('script');
script.src = url;
document.body.appendChild(script);

let selectShip = document.getElementById("selectShip");

function showAvailableShip(dis){
    //this function will load all ship from local storage and modifying the html in order to display.
    let display = "";
    for(let i = 0; i < shipArr.length; i++){
        if(shipArr[i]._range >= dis){ 
            display += "<option>";
            display += shipArr[i].name;
            display += "</option>"
        }
    }
    selectShip.innerHTML = "<option></option>" + display;
}

// calculate distance
function distanceCal(lon1, lat1, lon2, lat2) {
    //This function will return the distance between two position.
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}
// change unit
function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function showRoute(){
    let title = document.getElementById("title");
    let body = document.getElementById("body");
    for(let i = 0; i < shipArr.length; i++){
        if(selectShip.value === shipArr[i]._name){
            time = totalDis/shipArr[i]._maxSpeed
            cost = totalDis*shipArr[i]._cost;
        }
    }
    
    title.innerHTML = "<th class='mdl-data-table__cell--non-numeric'>Ship</th><th class='mdl-data-table__cell--non-numeric'>Distance</th><th class='mdl-data-table__cell--non-numeric'>Cost</th><th class='mdl-data-table__cell--non-numeric'>Time</th>";
    
    body.innerHTML = "<td class='mdl-data-table__cell--non-numeric'>" + selectShip.value + "</td>" + "<td class='mdl-data-table__cell--non-numeric'>" + totalDis.toFixed(2) + "Km</td>" + "<td class='mdl-data-table__cell--non-numeric'>$" + cost.toFixed(2) + "</td>" + "<td class='mdl-data-table__cell--non-numeric'>" + time.toFixed(2) + "Hour</td>";
    
    
    
}
let ROUTE_STORAGE_KEY = "ROUTE_STORAGE_BY_USER";

function saveRoute(){
    let name = prompt("Please Enter the name of route");
    let startDate = document.getElementById("sample1").value;
    let newRoute = new Route(name, selectShip.value, startPort.value, endPort.value, totalDis, time, cost, startDate, pointList, sourcePort.value, destinationPort.value);
    
    routeList._routeList.push(newRoute);//error...
    
    localStorage.setItem(ROUTE_STORAGE_KEY, JSON.stringify(routeList._routeList));
    window.location.reload();
}

function checkPoint(arr){
    //It is a fountion that check each point is further than 100km or not.
    var theArr = arr;
    for(let i = 0; i < theArr.length-1; i++){
        if(distanceCal(theArr[i][0], theArr[i][1], theArr[i+1][0], theArr[i+1][1]) < 100){
            return false;
        }
    }
    return true
}



