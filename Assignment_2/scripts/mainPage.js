let PORT_STORAGE_KEY = "PORT_STORAGE";
let pKEY = "PORT_INFO"
let info = '';

function sendToLocal(ele){
    //console.log(ele);
    //due to the numbers of port are massive, this fucntion is used to storage all port and html code to local storage so that we could use the stuff directly agter.
    for(let i = 0; i < ele['ports'].length; i++){
        let aPort = new Port(ele['ports'][i].name, ele['ports'][i].country, ele['ports'][i].type, ele['ports'][i].size, ele['ports'][i].lat, ele['ports'][i].lng);
        portList.addPort(aPort);
        info += "<tr><td class='mdl-data-table__cell--non-numeric'>Name:" + aPort._name + "</td>" +"<td class='mdl-data-table__cell--non-numeric'>Country:" +  aPort._country + "</td>" +"<td class='mdl-data-table__cell--non-numeric'>Type:" + aPort._type + "</td>" +"<td class='mdl-data-table__cell--non-numeric'>Size:" + aPort._size + "</td>" + "<td class='mdl-data-table__cell--non-numeric'>Latitude:" + aPort._latitude + "</td>" + "<td class='mdl-data-table__cell--non-numeric'>Longitude:" + aPort._longitude + "</td></tr>";

    }
    localStorage.setItem(PORT_STORAGE_KEY, JSON.stringify(portList._portList));
    localStorage.setItem(pKEY, info);
    //console.log(portList._portList[1]);
}

let portData = {
    callback: "sendToLocal"
}

jsonpRequest("https://eng1003.monash/api/v1/ports/", portData);



// For the part of add Route....

let allPorts = []
let allCountries = []
let temp = JSON.parse(localStorage.getItem("PORT_STORAGE"));
if(temp !== null){ 
    for(let i = 0; i < temp.length; i++){
        allPorts.push(temp[i]._name);
        allCountries.push(temp[i]._country);
    }
    allCountries = new Set(allCountries);
    allCountries = Array.from(allCountries);
    allCountries.sort();
}

let startPort = document.getElementById("startPort");
let endPort = document.getElementById("endPort");
let s = '';
let e = '';
for(let i = 0; i < allCountries.length; i++){
    s += "<option>" + allCountries[i] + "</option>";
    e += "<option>" + allCountries[i] + "</option>";
}

localStorage.setItem("startP", JSON.stringify(s));
localStorage.setItem("endP", JSON.stringify(e));
localStorage.setItem("allPort", JSON.stringify(allPorts));



let route = []
route = JSON.parse(localStorage.getItem("ROUTE_STORAGE_BY_USER"));


let disP = document.getElementById("routeB");
if(route !== null){ 
    for(var i= 0; i < route.length;i++){
        if(route[i] !== null){
            let name = route[i]._name;
            let cost = route[i]._cost;
            let destinationPort = route[i]._destinationPort;
            let sourcePort = route[i]._sourcePort;
            let time = route[i]._time;
            let totalDis = route[i]._distance;
            let startPortName = route[i]._startPortName;
            let destinationPortName = route[i]._destinationPortName;

        disP.innerHTML += "<tr><td onclick=\"clickToViewRoute("+i+")\" class=\"mdl-data-table__cell--non-numeric\">Name: " + name + "</br>" + sourcePort + " &rarr; " + destinationPort + "  Cost: $" + cost.toFixed(2) + "  Distance: "  + totalDis.toFixed(2) + "  Time: " + time.toFixed(2) + "Hours</td></tr>";
        }
    }

}
function clickToViewRoute(routeIndex){
    //this function will allow user to click route then show the info of this route, it also would go to viewRoute page automatically.
 let arr = [route[routeIndex]._name, route[routeIndex]._cost, route[routeIndex]._destinationPort, route[routeIndex]._sourcePort, route[routeIndex]._time, route[routeIndex]._distance, route[routeIndex]._startDate, route[routeIndex]._wayPointList, route[routeIndex]._startPortName, route[routeIndex]._destinationPortName]
 
localStorage.setItem("viewRouteInfo", JSON.stringify(arr))
    
    window.location.href = 'viewRoute.html';
}



//you want reload once after first load
window.onload = function() {
    //considering there aren't any hashes in the urls already
    if(!window.location.hash) {
        //setting window location
        window.location = window.location + '#loaded';
        //using reload() method to reload web page
        window.location.reload();
    }
}

