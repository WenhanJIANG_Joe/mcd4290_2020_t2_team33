
mapboxgl.accessToken = 'pk.eyJ1IjoiamlhYmluIiwiYSI6ImNrOWRwOGpiZjA0YWwzZWxsM3J5Z280aXoifQ.t_1iM47Zq06wEArTyO2rJQ';
    
    
var map = new mapboxgl.Map({
    
    container: 'map',
    
    style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
   
    center: [0, 0], // starting position [lng, lat]
    
    zoom: 1 // starting zoom
    
});

//let pointList = 0;
//let route = []
//route = JSON.parse(localStorage.getItem("ROUTE_STORAGE_BY_USER"));
let info = JSON.parse(localStorage.getItem("viewRouteInfo"));
//console.log(info);

let disP = document.getElementById("name");
    let name = info[0]
    let cost = info[1]
    let destinationPort = info[2]
    let sourcePort = info[3]
    let time = info[4]
    let totalDis = info[5]
    let startDate = info[6]
    let pointList = info[7]
    let sourcePortName = info[8]
    let destinationPortName = info[9]
    disP.innerHTML = "<tr><td class=\"mdl-data-table__cell--non-numeric\">" + name + "</td><td class=\"mdl-data-table__cell--non-numeric\">" + totalDis.toFixed(2) + " Km" +  "</td>" + "<td class=\"mdl-data-table__cell--non-numeric\">Cost: $" + cost.toFixed(2) + "</td><td class=\"mdl-data-table__cell--non-numeric\">startDate: " + startDate + "</td><td class=\"mdl-data-table__cell--non-numeric\">Time: " + time.toFixed(0) + " Hours</td></tr>";
    
    map.on('load', function() {

        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': pointList
                }
            }
        });

        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#666',
                'line-width': 4
            }
        });
    })
for(let i = 0; i < pointList.length; i++){
        if(i === 0){
            marker = new mapboxgl.Marker({color: 'green'})
            .setLngLat(pointList[i])
            .addTo(map)
            
            var popup = new mapboxgl.Popup({ closeOnClick: false })
            .setLngLat(pointList[i])
            .setHTML(sourcePortName+", "+sourcePort)
            .addTo(map);
        }
        else if(i === pointList.length-1){
            marker = new mapboxgl.Marker({color: 'red'})//should be red
              .setLngLat(pointList[pointList.length-1])
              .addTo(map);
            var popup = new mapboxgl.Popup({ closeOnClick: false })
            .setLngLat(pointList[i])
            .setHTML(destinationPortName+", "+destinationPort)
            .addTo(map);
        
        }
        else{
            marker = new mapboxgl.Marker({ color: 'orange'})
            .setLngLat(pointList[i])
            .addTo(map)
        }
    }


function deleteRoute(){
    //this function will delete current route when pressing the delete button.
    let getRoute = [];
    getRoute = JSON.parse(localStorage.getItem("ROUTE_STORAGE_BY_USER"));
    //console.log(info);
    //console.log(getRoute[0]._name);
    for(let i = 0; i < getRoute.length; i++){
        if(getRoute[i] !== null){
            if(getRoute[i]._name === info[0] && getRoute[i]._distance === info[5]){
            getRoute[i] = null;
            }
        }
    
    }
    console.log(getRoute);
    localStorage.removeItem("ROUTE_STORAGE_BY_USER")
    localStorage.setItem("ROUTE_STORAGE_BY_USER", JSON.stringify(getRoute))
    window.location.href = 'index.html';
}





