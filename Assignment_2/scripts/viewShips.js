

// start>>>
function showShip(obj){
    //show ship by modifying html
    let viewShipArea = document.getElementById("viewShipArea")
    viewShipArea.innerHTML += "<div class='mdl-cell mdl-cell--4-col'><div><h5>" 
        + obj._name + " @ $" + obj._cost +" per km</h5></div><div> Max Speed: " 
        + obj._maxSpeed + "</div><div> Range: " 
        + obj._range + "</div><div> Description: " 
        + obj._description + "</div><div> Status: " 
        + obj._status + "</div><div> Comments: " 
        + obj._comments + "</div></div>";

}

let theShip = JSON.parse(localStorage.getItem("SHIP_STORAGE"));
if (theShip !== null){
    for(let i = 0; i < theShip.length; i++){
    let newShip = new Ship();
    newShip._shipInitalize(theShip[i]);
    shipList._shipList.push(newShip);
    showShip(newShip);
    }
}


function callBackFunc(ele){ 
    //displaying the ship by changing html after requesting ship API.
    for(let i = 0; i < ele['ships'].length; i++){
        viewShipArea.innerHTML += "<div class='mdl-cell mdl-cell--4-col'><div><h5>" 
        + ele['ships'][i].name + " @ $" + ele['ships'][i].cost +" per km</h5></div><div> Max Speed: " 
        + ele['ships'][i].maxSpeed + "</div><div> Range: " 
        + ele['ships'][i].range + "</div><div> Description: " 
        + ele['ships'][i].desc + "</div><div> Status: " 
        + ele['ships'][i].status + "</div><div> Comments: " 
        + ele['ships'][i].comments + "</div></div>";
    }
}

jsonpRequest("https://eng1003.monash/api/v1/ships/", shipAPIData);

